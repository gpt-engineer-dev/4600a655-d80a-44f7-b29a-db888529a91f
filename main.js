document.addEventListener('DOMContentLoaded', () => {
    const addButton = document.getElementById('add-todo');
    const inputField = document.getElementById('todo-input');
    const todoList = document.getElementById('todo-list');

    addButton.addEventListener('click', () => {
        const todoText = inputField.value.trim();
        if (todoText) {
            addTodoItem(todoText);
            inputField.value = ''; // Clear the input field
        }
    });

    function addTodoItem(text) {
        const listItem = document.createElement('li');
        listItem.className = 'flex justify-between items-center bg-white p-2 my-2 shadow-sm';

        const span = document.createElement('span');
        span.textContent = text;

        const deleteButton = document.createElement('button');
        deleteButton.innerHTML = '<i class="fas fa-trash-alt"></i>';
        deleteButton.className = 'delete-todo text-red-500 hover:text-red-700';

        deleteButton.addEventListener('click', () => {
            todoList.removeChild(listItem);
        });

        listItem.appendChild(span);
        listItem.appendChild(deleteButton);

        todoList.appendChild(listItem);
    }
});
